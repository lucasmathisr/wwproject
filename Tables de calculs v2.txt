Tables de calculs V2 pour projet WW

Calculs de pré-tour :
	Nb de tuiles : NbT
	Nb de batiments : NbBat
	Nb d'usines : NbU
	Argent dans la reserve : CR
	
	Argent du tour = CR + (NbT - NbBat) * 1500 + NbU * 2500
	
	
	
Calculs de fin de tour :
	Argent restant du tour : Rest
	Nb de banques : NbBan
	
	Capacité reserve : RCap
	
	RCap = 15 000 + NbBan * 10 000
	
	Reserve = Rest if (RES <= RCap) else RCap
	
	
---

Calculs du coût RP :
	1 RP = 500 crédits

Calculs coût de recherche véhicules :
	2 RP pour les véhicules de premier rang
	4 RP pour les autres

Coût d'achat des véhicules:
	https://docs.google.com/spreadsheets/d/1HN2zSLUOsPmVvW73yj9S9Ew0805j1iLllY4KUeOh4VY/edit?usp=sharing
	Dépend du type de véhicule et du BR uniquement
	
---
