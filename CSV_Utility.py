# -*- coding: utf-8 -*-
"""

    @author: Lucas ROUELLE (redace)

"""

"""

    Utility classes for CSV handling

"""

def testFileOpening(fileName):
    with open(fileName, 'r') as f:
        try:
            f.readlines()
            eof = f.tell()
        except Exception as e:
            print(f'Error While trying to read "{fileName}",',
                  "\nMake sure the file exists in the same directory as the program",
                  e.message())
            eof = False
    return eof


def getCSVLine(file):
    lineSplit = file.readline().split("\n")[0]
    lineSplit = lineSplit.split(",")
    while lineSplit[-1] == "" and len(lineSplit) > 1:
        lineSplit.pop(-1)
    return lineSplit


def printDict(Dict : dict, level=0):
    print()
    for i in Dict:
        j = Dict[i]
        print(level*'\t' + f"{i} :", end=" ")
        if type(j) == dict:
            printDict(j, level+1)
        else:
            print(j)
