# -*- coding: utf-8 -*-
"""
Created on Tue Dec 31 10:46:22 2019

@author: Lucas ROUELLE (redace)
"""

"""

    Functions Declaration
    --------------------
    CSV Numerisation
    
"""
def printDict(Dict):
    print("\nArrays :")
    for i in Dict: print(i);

def printParameters(parameters):
    print("\nParameters :")
    for i in parameters: print(i, "=", parameters[i]);

def testFileOpening(fileName):
    try:
        f = open(fileName, 'r')
        f.readlines()
        eof = f.tell()
        print("EOF at" , eof)
    except:
        print("Error While trying to read \""+ fileName
              +"\"\nMake sure the file exists in the same directory as the programm")
        eof = False;
    finally:    
        try: f.close();
        except: pass;
    return eof

def getCSVLine(file):
    lineSplit = file.readline().split("\n")[0]
    lineSplit = lineSplit.split(";")
    while lineSplit[-1] == "" and len(lineSplit) > 1:
        lineSplit.pop(-1)
    return lineSplit

def getCSVDictToValue(line):
    key = line[0]
    values = line[1:]
    return [{key : i} for i in values]

def getCSVLineKey(file):
    return getCSVLine(file)[0]

def getCSVLineValues(file):
    return getCSVLine(file)[1:]

def NumeriseCSV(fileName, convert):
    print("Numerising \"" + fileName + '"')
    eof = testFileOpening(fileName)
    if eof == False: return False;
    with open(fileName, 'r', encoding='utf-8') as f:
        nTags = 0;
        Tableau = [];
        endOfTab = False;
        
        while(True):
            if f.tell() == eof:
                break
            line = getCSVLine(f)
            size = len(line)
            if size == 0:
                continue
            elif size == 1:
                if not endOfTab:
                    print("End of array\nReading parameters")
                    endOfTab = True;
                
                parameters = line[0].split("=")
                if len(parameters) != 1:
                    if parameters[0] != "":
                        if len(parameters) != 2:
                            print('Error : Invalid parameter line "' + line[0] + '" : skipped')
                            continue
                        if parameters[1] == "":
                            print("Empty value for parameter \"" + parameters[0] + "\" : skipped")
                            continue
                        if parameters[0] in convert:
                            convert[parameters[0]] = parameters[1]
                            print("Parameter \"" + parameters[0] +"\" value set to : " + parameters[1])
                            continue
                        else:
                            print('Unknown Parameter : "' + parameters[0] + '" skipped')
                            continue
                    else:
                        print("Empty Parameter name : skipped")
                else: continue;
            else:
                if endOfTab:
                    print("Error : Array line given after parameters !")
                    return False
                
                line = getCSVDictToValue(line)
                size -= 1
                
                if nTags == 0: nTags = size;
                
                if nTags != size:
                    print("Error : Number of colums is differents between lines !")
                    return False
                
                key = [key for key in line[0]][0]
                values = [i[key] for i in line]
                print("Line \"" + key + "\" :",values)
                if Tableau == []:
                    for i in range(nTags):
                        Tableau.append({})
                for i in range(nTags):
                    Tableau[i][key] = values[i]
                    
        print("End of file")
        
        return [Tableau, convert]
        
        
def checkParameters(convert):
    print('Checking parameters')
    validParameters = True
    
    for i in convert:
        if convert[i] == None:
            print('Error : Missing necessary Value : "' + i + '"' )
            validParameters = False
    
    if not validParameters:
        return False
    
    print("All necessary parameters have been set !")
    return True

def arrangePolitics(politics):
    arranged = {}
    for i in politics:
        arranged[i["Politic"]] = {
            "CR":int(i["CR Mod."]),
            "PR":int(i["PR Mod."]),
            "RP":int(i["RP Mod."]),
            }
    return arranged

def arrangeCountries(tableau):
    arranged = {}
    for i in tableau:
        arranged[i["Country"]] = {
            "Base CR":int(i["Base CR"]),
            "Base PR":int(i["Base PR"]),
            "Base RP":float(i["Base RP"]),
            "Politic":i["Politic"]
            }
    return arranged
"""

    Calculation functions

"""

def addPolitics(tableau, politics):
    out = {}
    for i in tableau:
        line = tableau[i]
        politic = politics[line["Politic"]]
        out[i] = {
            "CR":(int(line["Base CR"]*(1 + politic["CR"] / 100))),
            "PR":(int(line["Base PR"]*(1 + politic["PR"] / 100))),
            "RP":(line["Base RP"]*(1 + politic["RP"] / 100)),
            "Politic":line["Politic"]
            }
    return out

def addBonusRessources(tableau, bonus):
    return tableau

def printTurnDict(Dict):
    for i in Dict:
        print(i + " :", Dict[i])

"""

    InitVariables
    
"""

countryTableFileName = "countryTable.csv"
politicsTableFileName = "politics.csv"
fileOutName = "OUT.csv"


"""

    Main Script
    
"""

def main():
    print("Start")
    convert = {
            "turn" : None,
            "undefinedParameter" : None,
            }
    result = NumeriseCSV(countryTableFileName, convert)
    if result == False:
        return
        
    tableau, convert = result
    
    if checkParameters(convert) == False:
        return
        
    printDict(tableau)
    printParameters(convert)
    tableau = arrangeCountries(tableau)
    print('\n"' + countryTableFileName + "\" has been succesfully numerised")
    result = NumeriseCSV(politicsTableFileName, {})
    
    if result == False:
        return
        
    politics = result[0]
    printDict(politics)
    politics = arrangePolitics(politics)
    print('\n"' + politicsTableFileName + "\" has been succesfully numerised")
    print("End of reading : no error")
    print("Starting calculations\nAdding politic factors")
    preTurn = addPolitics(tableau, politics)
    print("PreTurn values :\n")
    printTurnDict(preTurn)
    print("End")

if __name__ == "__main__":
    main()
    