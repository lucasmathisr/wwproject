# -*- coding: utf-8 -*-
"""

    @author: Lucas ROUELLE (redace)

"""

from CSV_Utility import *

FILENAME = "countryTable.csv"

"""

    Functions Declaration
    --------------------
    CSV Conversion

"""


def printParameters(parameters):
    print("\nParameters :")
    [print(i, "=", parameters[i]) for i in parameters]


def getCSVDictToValue(line):
    key = line[0]
    values = line[1:]
    return [{key: i} for i in values]


def getCSVLineKey(file):
    return getCSVLine(file)[0]


def getCSVLineValues(file):
    return getCSVLine(file)[1:]


def ConvertCSV(fileName, convert):
    print(f'Converting "{fileName}"')
    eof = testFileOpening(fileName)
    if not eof:
        return False
    with open(fileName, 'r', encoding='utf-8') as f:
        nTags = 0
        Tableau = []
        endOfTab = False

        while True:
            if f.tell() == eof:
                break
            line = getCSVLine(f)
            size = len(line)
            if size == 0:
                continue
            elif size == 1:
                if not endOfTab:
                    print("End of array\nReading parameters")
                    endOfTab = True

                parameters = line[0].split("=")
                if len(parameters) != 1:
                    if parameters[0] != "":
                        if len(parameters) != 2:
                            print(f'Error : Invalid parameter line "{line[0]}" : skipped')
                            continue
                        if parameters[1] == "":
                            print(f'Empty value for parameter "{parameters[0]}" : skipped')
                            continue
                        if parameters[0] in convert:
                            convert[parameters[0]] = parameters[1]
                            print(f'Parameter "{parameters[0]}" value set to : {parameters[1]}')
                            continue
                        else:
                            print(f'Unknown Parameter : "{parameters[0]}" skipped')
                            continue
                    else:
                        print("Empty Parameter name : skipped")
                else:
                    continue
            else:
                if endOfTab:
                    print("Error : Array line given after parameters !")
                    return False

                line = getCSVDictToValue(line)
                size -= 1

                if nTags == 0:
                    nTags = size

                if nTags != size:
                    print("Error : Number of columns is different between lines !")
                    return False

                key = [key for key in line[0]][0]
                values = [i[key] for i in line]
                print(f'Line "{key}" : {values}')
                if not Tableau:
                    for i in range(nTags):
                        Tableau.append({})
                for i in range(nTags):
                    Tableau[i][key] = values[i]

        print("End of file")

        return [Tableau, convert]


def checkParameters(convert):
    print('Checking parameters')
    validParameters = True

    for i in convert:
        if convert[i] is None:
            print(f'Error : Missing necessary Value : "{i}"')
            validParameters = False

    if not validParameters:
        return False

    print("All necessary parameters have been set !")
    return True


def arrangeCountries(tableau):
    arranged = {}
    for i in tableau:
        arranged[i["Country"]] = {
            "Base CR": int(i["Credits"]),
            "Tiles": int(i["Tiles quantity"]),
            "Factories": int(i["Factory quantity"]),
            "Banks": int(i["Bank quantity"]),
            "Buildings": int(i["Building quantity"])
        }
    return arranged


def computePreTurnRessources(tableau: dict):
    preTurnValues = {}

    for i in tableau:
        j = tableau[i]
        preTurnValues[i] = {"Turn CR": j["Base CR"] + (j["Tiles"] - j["Buildings"]) * 1500 + 2500 * j["Factories"],
                            "Reserve Max": (15000 + j["Banks"] * 10000)}

    return preTurnValues


if __name__ == "__main__":
    convert = {
        "turn": None,
        "undefinedParameter": None,
    }
    result = ConvertCSV(FILENAME, convert)
    if not result:
        quit()

    tableau, convert = result

    if not checkParameters(convert):
        quit()

    printParameters(convert)
    tableau = arrangeCountries(tableau)
    print(f'{FILENAME} has been successfully converted')
    printDict(tableau)
    preTurnValues = computePreTurnRessources(tableau)
    printDict(preTurnValues)
    print("Turn successfully prepared")

    print("> End of script")
