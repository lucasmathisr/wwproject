# -*- coding: utf-8 -*-
"""

Created on Mon Jul 13 19:20:11 2020

@author: redace

"""


#
# Interface de ligne de commandes
#

# Classe de référence pour les erreurs de commandes
class CommandError(Exception):
    message = "Erreur de commande"

    pass


# Classe liée à une entrée vide de commande
class NoCommand(CommandError):
    def __init__(self):
        self.message = "Entrez une commande"
        super().__init__(self.message)


# Classe liée à une erreur dans les arguments fournis à un objet de commande
class ArgumentError(CommandError):
    def __init__(self, arg, command):
        self.message = f'L\'argument "{arg}" n\'est pas un argument valide pour la commande "{command}"'
        super().__init__(self.message)


# Classe liée à une erreur de commande non-trouvée
class CommandNotFound(CommandError):
    def __init__(self, command):
        self.message = f'La commande "{command}" n\'existe pas !'
        super().__init__(self.message)


# Classe de l'objet de gestion d'une interface en ligne de commande basique
class CommandLineInterface:
    commandList = {}  # Dictionnaire des objets de commande
    isActive = False  # Etat de la boucle de capture de saisie et du bloc
    userText = ""  # Stockage de la frappe utilisateur
    command = None  # Objet de la commande sélectionnée
    inputPhrase = "> "
    interface = None  # Objet de l'interface maître

    def __init__(self, commandList: list):
        # Import de la liste des commandes de l'interface
        for c in commandList:
            self.commandList[c.name] = c  # Relie l'objet de commande à son nom d'appel

    # Lance la capture de saisie utilisateur
    def active(self):
        self.isActive = True  # Vérouillage de la boucle
        while self.isActive:
            try:
                self.userText = input(self.inputPhrase)  # Capture de saisie
                self.rmvBlank()  # Retire les espace en trop avant la commande
                self.cutUserText()  # Coupe la saisie selon les espaces pour isoler les commandes
                self.setCommand()  # Trouve et vérouille la commande
                self.launchCommand()  # Lance la commande vérouillée
            except CommandError as E:  # Intercepte les erreurs de commande
                print(E.message)  # Affiche le message d'erreur de la commande avant de reboucler

    # Vérifie si la saisie donnée est vide et renvoie une erreur si oui
    def checkEmpty(self):
        if len(self.userText) == 0:
            raise NoCommand

    # Retire les espace en trop et vérifie que le texte n'est pas vide
    def rmvBlank(self):
        while True:
            self.checkEmpty()  # Vérifie si le texte n'est pas vide
            if self.userText[0] == " ":
                self.userText = self.userText[1:]  # Retire l'espace détecté en première position
            else:
                break  # Sort si aucun espace détecté en première position

    # Découpe la frappe de l'utilisateur en mots de commande
    def cutUserText(self):
        userText = (self.userText.split(" "))  # Coupage au niveau des espaces
        if userText[0] == self.userText:  # Si il n'y a qu'un seul mot, le mot est immédiatement transféré
            finalText = userText

        else:  # Lance la recherche des mots
            finalText = []
            for i in userText:  # Parcours les mots de la découpe
                if i != "":  # Détecte les coupes vides (espaces en trop dans la frappe utilisateur)
                    finalText.append(i)
        self.userText = finalText  # Définie le nouveau texte découpé

    # Vérouille l'objet associée à la commande si trouvée dans la liste des commandes
    def setCommand(self):
        if not self.commandList.get(self.userText[0], False):
            raise CommandNotFound(self.userText[0])  # Lève une erreur si la commande n'est pas trouvée
        else:
            self.command = self.commandList[self.userText[0]]  # Récupère l'objet de la commande
            self.command.args = self.userText[1:]  # Envoie le reste des arguments dans l'objet de commande
            self.command.setInterface(self)  # Envoie l'objet de l'interface dans l'objet de commande

    # Execute la commande vérouillée
    def launchCommand(self):
        self.command.execute()


# Classe des objets de commande
class CommandLine:
    name = "command"  # Nom utilisé pour appeller la commande
    interface = None  # Objet de l'interface invoquant la commande
    args = []  # Liste des arguments éventuels
    helpMsg = "Ceci est un message d'aide pour la commande"  # Message d'aide de la commande

    def __init__(self):
        pass

    # Vérification des arguments si nécessaire
    def _argCheck(self, args: dict):
        # Vérifie les arguments et prépare l'execution de la commande
        arg = "arg"
        argIsValid = True
        process = {"noArg": True}
        if not argIsValid:
            raise ArgumentError(arg, self.name)

    # Lance le script de la commande
    def execute(self):
        pass

    # Retourne le mesage d'aide de la commande
    def getHelpMsg(self):
        return self.helpMsg

    # Défini l'objet de l'interface pour la commande
    def setInterface(self, interface):
        self.interface = interface


# Objet de commande help
# Renvoie un message d'erreur générique ou l'aide d'un autre objet de commande si un argument valide est fourni
class HelpCommand(CommandLine):
    interface = None
    name = "help"
    helpMsg = "Tapez une commande et ses éventuels arguments pour l'executer"

    def execute(self):
        if len(self.args) == 0:  # Si il n'y a pas d'arguments fournis, affiche le message d'aide de help
            print(self.helpMsg)
        else:
            self.interface.userText = self.args[0]  # Charge l'argument de nom de commande dans l'interface
            self.interface.cutUserText()
            self.interface.setCommand()  # Charge l'objet de commande indiqué
            print(self.interface.command.helpMsg)  # Affiche le message d'aide de la commande sélectionnée


# Passe l'interface hôte de la commande en non actif ce qui la sort de sa boucle
class ExitCommand(CommandLine):
    interface = None
    name = "exit"
    helpMsg = "Quitte l'interface actuelle et retourne à la précédente"

    # Change l'état de l'interface hôte en inactif
    def execute(self):
        interface = self.interface
        if interface.interface is None:
            print("Exiting ...")
        interface.isActive = False


class LoginCommand(CommandLine):
    interface = None
    name = "login"
    helpMsg = "Permet l'authentification utilisateur\nFormat : login [username]"

    def execute(self):
        pass


if __name__ == "__main__":
    CMD = CommandLineInterface([HelpCommand(), ExitCommand()])
    CMD.active()
