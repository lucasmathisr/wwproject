# -*- coding: utf-8 -*-
"""

    @author: Lucas ROUELLE (redace)

"""

from CSV_Utility import *

priceTableName = "VehiclePriceTable.csv"
vehicleTableName = "Vehicle Table - #TYPE#.csv"
vehicleTypeImportOverride = {"Ground": True,
                             "Air": True,
                             "Naval": True,
                             "Helicopter": True}
nationNameCodeConversion = {
    "F": "France",
    "J": "Japan",
    "D": "Germany",
    "I": "Italy",
    "CN": "China",
    "GB": "UK",
    "SE": "Sweden",
    "USSR": "Russia",
    "USA": "United States"}

"""

    Classes for the main calculations

"""

"""

    Classes for vehicles handling

"""


def importVehiclePriceList():
    eof = testFileOpening(priceTableName)
    if not eof:
        return False

    with open(priceTableName, 'r') as file:
        line = getCSVLine(file)
        types = line[1:]
        VPrices = {}
        for i in types:
            VPrices[i] = {}

        while file.tell() != eof:
            line = getCSVLine(file)
            for i in range(1, len(line)):
                VPrices[types[i - 1]][str(line[0])] = int(line[i])

    return VPrices


vehiclePriceList = importVehiclePriceList()


def importVehicleList():
    VTable = {}
    for key in vehicleTypeImportOverride:
        if vehicleTypeImportOverride[key]:
            tableName = vehicleTableName.split("#TYPE#")
            tableName = tableName[0] + key + tableName[1]
            eof = testFileOpening(tableName)
            if not eof:
                print(f"Error while trying to import {tableName}")
                continue

            with open(tableName, 'r') as file:
                line = getCSVLine(file)
                types = line[1:]
                table = {}

                while file.tell() != eof:
                    line = getCSVLine(file)

                    if "N/A" in line:
                        continue

                    element = {}
                    for i in range(1, len(line)):
                        element[types[i - 1]] = line[i]

                    table[line[0]] = element
            VTable[key] = table
    return VTable


vehicleTable = importVehicleList()


def getRPrice(tier):
    return 2 if tier == 1 else 4


class Vehicle:
    def __init__(self, name: str, nation: str, vehicleType: str, tier: int, BR: str):
        self.name = name
        self.nation = nation
        self.vehicleType = vehicleType
        self.tier = tier
        self.BR = BR
        self.RPrice = getRPrice(tier)
        self.price = vehiclePriceList[vehicleType][BR]

    def __str__(self):
        return f"Name : {self.name}\n" \
               f"Nation : {self.nation}\n" \
               f"Vehicle Type : {self.vehicleType}\n" \
               f"Tier : {self.tier}\n" \
               f"BR : {self.BR}\n" \
               f"Research Price : {self.RPrice}\n" \
               f"Price : {self.price}"


def generateVehicleObjList():
    VList = []
    for i in vehicleTable:
        VTypeTable = vehicleTable[i]
        for j in VTypeTable:
            args = VTypeTable[j]
            vehicle = Vehicle(j, nationNameCodeConversion[args['Nation']], i, args["Tier"], args["BR"])
            VList.append(vehicle)
    return VList


vehicleList = generateVehicleObjList()


def printVehicleList(VList):
    for i in VList:
        print(f"{i}\n")


class VehicleEntityContainer:
    def __init__(self, VList):
        self.container = {}
        self.VList = VList

    def addEntity(self, VEID: str, VType: Vehicle, tile: MapTile, nation: Country):
        if self.container.get(VEID) is not None:
            print(f"A vehicle with VEID {VEID} already exist in the list !"
                  f"Vehicle entity was not added")
        else:
            self.container[str(VEID)] = VehicleEntity(self, VEID, VType, tile, nation)

    def createEntity(self, VType: Vehicle, tile: MapTile, nation: Country):
        keyList = container.keys()
        for i in range(max(keyList)):
            if str(i) not in keyList:
                self.addEntity(str(i), VType, tile, nation)
                break

    def findEntitiesOnTile(self, tile):
        EList = []
        for i in self.container:
            if self.container[i].tile == tile:
                EList.append(self.container[i])
        return None if empty(EList) else EList

    def getEntity(self, VEID: str):
        return self.container.get(VEID)


class VehicleEntity:
    def __init__(self, VEList: VehicleEntityContainer, VEID: str, VType: Vehicle, tile: MapTile, nation: Country):
        self.VType = VType
        self.VEList = VEList
        self.VEID = VEID
        self.tile = tile
        self.nation = nation

    def move(self, newTile):
        self.tile = newTile


def testVehicles():
    priceTable = vehiclePriceList
    printDict(priceTable)
    print()
    vehicle = Vehicle("AMX-13", "France", "Ground", 2, "3.7")
    print(vehicle.getVehicleStr())
    print()


"""

    Classes for country handling

"""


class Country:
    def __init__(self, countryId: int, name: str, CR: int, RP: int, vehicles=None, researchState=None):
        self.countryId = countryId
        self.name = name
        self.CR = CR
        self.RP = RP
        self.researchState = researchState


"""

    Classes for map tiles handling

"""


class MapTile:
    def __init__(self, position: (float, float), tileId: float, name: str, region, owner: Country = None,
                 vehicles: list = None):
        self.position = position
        self.tileId = tileId
        self.name = name
        self.region = region
        self.owner = owner
        self.vehicles = vehicles

    def __str__(self):
        return f"Map Tile n°{self.tileId}\n" \
               f"Name : {self.name}\n" \
               f"Location ({self.position[0]}x{self.position[1]}) in region {self.region}\n" \
               f"Owner : {'None' if self.owner is None else self.owner.name}\n" \
               f"Vehicles inside : {'No' if self.vehicles is None else 'Yes'}"


class LandMapTile(MapTile):
    def __init__(self, position: (float, float), tileId: float, name: str, region, coast: bool, owner: Country = None,
                 vehicles: list = None, building=None):
        super().__init__(position, tileId, name, region, owner, vehicles)
        self.coast = coast
        self.building = building

    def __str__(self):
        return super().__str__() + f"\nIs a coast : {'Yes' if self.coast else 'No'}\n" \
                                   f"Building : {'None' if self.building is None else self.building}"


def testTiles():
    country = Country(5, "France", 10000, 10)
    tile = LandMapTile((100, 100), 10, "test", "France", True, country)
    print(tile)


if __name__ == "__main__":
    printVehicleList(vehicleList)

    print("done")
