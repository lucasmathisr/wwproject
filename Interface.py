# -*- coding: utf-8 -*-
"""

@author: redace

"""

# https://www.dreamincode.net/forums/topic/401541-buttons-and-sliders-in-pygame/

import sys

try:
    import pygame
except Exception as e:
    print(f"Impossible d'importer pygame !\n{e}")
    exit()

# Liste des couleurs
colorList = {
    "RED": (255, 0, 0),
    "GREEN": (0, 255, 0),
    "BLUE": (0, 0, 255),
    "BLACK": (0, 0, 0),
    "WHITE": (255, 255, 255),
    "GREY": (128, 128, 128),
    "L_GREY": (200, 200, 200)
}


# Classe des boutons d'interaction
class Button:
    def __init__(self, txt, location=(100, 100), bg=colorList["WHITE"], fg=colorList["BLACK"], size=(80, 30),
                 font_name="Segoe Print", font_size=16):
        self.color = bg  # the static (normal) color
        self.bg = bg  # actual background color, can change on mouseover
        self.fg = fg  # text color
        self.size = size

        self.font = pygame.font.SysFont(font_name, font_size)
        self.txt = txt
        self.txt_surf = self.font.render(self.txt, 1, self.fg)
        self.txt_rect = self.txt_surf.get_rect(center=[s // 2 for s in self.size])

        self.surface = pygame.surface.Surface(size)
        self.rect = self.surface.get_rect(center=location)
        self.hovered = False
        self.screen = None
        self.gui = None

    # Défini si le bouton est survolé ou non
    def setHovered(self, hovered):
        self.hovered = hovered

    # Dessine le bouton
    def draw(self):
        self._checkHoveringState()
        self.rect = self.surface.get_rect(center=[i // 2 for i in self.gui.size])

        self.surface.fill(self.bg)
        self.surface.blit(self.txt_surf, self.txt_rect)
        self.screen.blit(self.surface, self.rect)

    # Vérfie l'état de survol du bouton et défini les paramètres couleurs
    def _checkHoveringState(self):
        if self.hovered:
            self.bg = self.color
        else:
            self.bg = colorList["GREY"]  # mouseover color

    # Vérifie la collision avec les coordonnées données
    def checkCollision(self, pos):
        if self.rect.collidepoint(pos):
            return True

    # Action effectuée au clic sur le bouton
    def clickAction(self):
        print("I have been pressed !")

    # Définie l'écran contenant le bouton
    def setScreen(self, screen):
        self.screen = screen

    # Défini le GUI contenant le bouton
    def setGui(self, parentGui):
        self.gui = parentGui


# Classe du GUI de la fenêtre
# Utilisé pour la fenètre de pygame
class GUI:
    def __init__(self):
        pygame.init()  # Init de pygame
        self.size = (200, 200)  # Taille initiale de l'écran
        self.screen = pygame.display.set_mode(self.size, pygame.RESIZABLE)  # Création de l'écran
        self.objects = []  # Objets contenus dans l'écran

    # Gère les évènements dans la fenêtre
    def _handlePygameEvents(self):
        for event in pygame.event.get():
            eType = event.type

            # Event clic croix rouge ou action "quitter"
            if eType == pygame.QUIT:
                pygame.quit()
                print("Sortie du programme")
                sys.exit(0)

            # Event Resize de l'écran
            elif eType == pygame.VIDEORESIZE:
                self.size = (event.w, event.h)
                print(f"Screen resized to {self.size}")

            # Event clic gauche
            elif eType == pygame.MOUSEBUTTONDOWN:
                self._mouseClick()

    # Vérifie les events possibles (collisions et events)
    def checkEvents(self):
        self._checkMouseCollision()
        self._handlePygameEvents()

    # Dessine les élements de la fenêtre
    def draw(self):
        self.screen = pygame.display.set_mode(self.size, pygame.RESIZABLE)  # Recréé l'écran avec la taille à jour
        for obj in self.objects:
            obj.draw()
        pygame.display.flip()
        pygame.time.wait(40)

    # Lance les actions de clic sur les objets survolés
    def _mouseClick(self):
        for obj in self.objects:
            if obj.hovered:
                obj.clickAction()

    # Vérifie les collisions souris avec les objets
    def _checkMouseCollision(self):
        pos = pygame.mouse.get_pos()
        for obj in self.objects:
            hovered = True if obj.checkCollision(pos) else False
            obj.setHovered(hovered)

    # Ajoute un objet à la liste des objets de la fenêtre
    def addObject(self, obj):
        if not (obj in self.objects):
            obj.setScreen(self.screen)
            obj.setGui(self)
            self.objects.append(obj)
        else:
            return False


if __name__ == "__main__":

    gui = GUI()

    gui.addObject(Button("Test"))

    gui.draw()
    while True:
        gui.checkEvents()
        gui.draw()
