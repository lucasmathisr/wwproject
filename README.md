Welcome to the United World War server project for a visual application capable of calculating and managing automaticaly data from the mass role-playing game on the World War United Discord server.
This app is in semi-open development. I work on it on my free time and use it as a learnig project. Everyone willing to help is welcome.

The application is programmed in Python:
    - PyGame for graphic interface
    - Django will be used for website based functions

All CSV files are test files formatted correctly for the main program, you can modify them as you wish to try out the features (don't push them into the repo tho !)

Implemented features :
- CSV conversion
- Array writing to output CSV file
- Basic of command-line interface
- Basic of visual interface

On the way features :
- Update to new calculation rules
- Adding commands to command-line interface
- Vehicles research/buying
- In turn actions
- Code optimisation/simplification

Futures features :
- In-turn effect on resources (player choices)
- Use of database
- API system evolution
- Interactive map
- Website implementation
- More

--- Q & A ---

_Why a command-line system ?_

A command-line system makes it easier to manipulate the main program with complex arguments. Also using a command-line system make it easier to implement an API afterward

_Why only Python ?_

Why not ? I'm here to learn and experiment and I love Python. I want to see if it's possible to do a full app with only Python

_Why a web app later ? An app isn't enough ? Why not only a web app then ?_

Python isn't really fully developed in term of web-based "real-time launch" unlike what javascript is able to do. Python can be used as a sort of PHP without much trouble.
Having a PHP equivalent in Python would be cool but a little "non-interactive", like an old website without javascript.
With an app I can have the full Python potential right from the computer guts ! It seems like an in-browser "launch" of Python is possible, so developing an app and then trying to "launch" it directly in a browser seems a good solution for now.

_Why is everything in french ?_

I mean ... I'm french and I'm working alone right now. I'll be happy to translate everything to english if someone where to join me ;-)

_Can you explain why this project even exists ?_

A close friend of mine needed help to automate VERY LONG excel sheet of resources calculations for the UWW Mass RPG turns, the project evolved to this form after some refining of the objectives.
This was also an "excuse" to learn more things and have a real personal project to do

_Any project after this ?_

One evolution of the project would be to use this app to develop a platform able to help other Mass RPG games to exist without the heavy workforce necessary to maintain and run them

_Who are you ?_

I'm an engineering student from France and I live mainly in Paris. I like programing but I'm more into the embedded systems branch of engineering.